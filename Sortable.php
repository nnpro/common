<?php
/**
 * @package rentorder
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 28.10.13
 * @time 19:57
 */

namespace NNPro\Common;


interface Sortable
{
    /**
     * return an integer value indicating the sort order of this element
     * compared to the given one
     *
     * @return int < 0 if this element is "lower" then the given one, > 0 if greater and 0 if equal
     */
    public function compareTo(Sortable $other);
}