<?php
/**
 * @package rentorder
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 28.10.13
 * @time 19:57
 */

namespace NNPro\Common;


interface Comparable
{
    /**
     * @param Comparable $other
     * @return bool
     */
    public function equals(Comparable $other);
}